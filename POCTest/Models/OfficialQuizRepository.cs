﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using POCTest.API_Classes;

namespace POCTest.Models
{
    public class OfficialQuizRepository : IQuizRepository
    {
        public IEnumerable<Answer> GetAnswers()
        {
            using (HttpClient client = new HttpClient())
            {
                AnswersClient ac = new AnswersClient(client);

                Task<ICollection<Answer>> T;

                //List<Answer> allAnswers = new List<Answer>();

                int TaskTimeOut = Convert.ToInt32(Appconfig.GetConfigValue("TaskTimeOut"));

                T = ac.GetAnswersAsync();
                T.Wait(TaskTimeOut);

                return T.Result;
            }
        }

        public IEnumerable<Question> GetQuestions()
        {
            using (HttpClient client = new HttpClient())
            {
                QuestionsClient qc = new QuestionsClient(client);

                Task<ICollection<Question>> T;

                //List<Question> allQuestions = new List<Question>();

                int TaskTimeOut = Convert.ToInt32(Appconfig.GetConfigValue("TaskTimeOut"));

                T = qc.GetQuestionsAsync();
                T.Wait(TaskTimeOut);

                return T.Result;
            }
        }

        public Question GetQuestion(int Id)
        {
            using (HttpClient client = new HttpClient())
            {
                QuestionsClient qc = new QuestionsClient(client);

                Task<Question> T;

                int TaskTimeOut = Convert.ToInt32(Appconfig.GetConfigValue("TaskTimeOut"));

                T = qc.GetQuestionAsync(Id);
                T.Wait(TaskTimeOut);

                return T.Result;
            }
        }

        public void SubmitQuestion(Question q)
        {
            using (HttpClient client = new HttpClient())
            {
                QuestionsClient qc = new QuestionsClient(client);

                Task<Question> T;

                int TaskTimeOut = Convert.ToInt32(Appconfig.GetConfigValue("TaskTimeOut"));

                T = qc.PostQuestionAsync(q);
                T.Wait(TaskTimeOut);
            }
        }
    }
}
