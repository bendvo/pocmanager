﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using POCTest.API_Classes;

namespace POCTest.Models
{
    public class RESTfulQuestionsRepository : IQuestionRepository
    {   
        public IEnumerable<Question> GetQuestions()
        {
            using (HttpClient client = new HttpClient())
            {
                QuestionsClient qc = new QuestionsClient(client);

                Task<ICollection<Question>> T;

                List<Question> allQuestions = new List<Question>();

                int TaskTimeOut = 11000;

                T = qc.GetQuestionsAsync();
                T.Wait(TaskTimeOut);

                //try
                //{
                //    foreach (Question q in T.Result)
                //        allQuestions.Add(q);
                //}
                //catch(Exception e)
                //{
                //    throw (e);
                //}

                //return allQuestions;

                return T.Result;
            }
        }
    }
}
