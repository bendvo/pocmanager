﻿using POCTest.API_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POCTest.Models
{
    public interface IQuestionRepository
    {
        IEnumerable<Question> GetQuestions();
    }
}
