﻿using POCTest.API_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POCTest.Models
{
    public interface IQuizRepository
    {
        IEnumerable<Question> GetQuestions();
        IEnumerable<Answer> GetAnswers();
        Question GetQuestion(int Id);

        void SubmitQuestion(Question q);
    }
}
