﻿using POCTest.API_Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POCTest.ViewModels
{
    public class HomeViewModel
    {
        public string Title { get; set; }
        public List<Question> Questions { get; set; }
        public List<Answer> Answers { get; set; }

        public Question _question { get; set; }
    }
}
