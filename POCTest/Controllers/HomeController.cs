﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using POCTest.API_Classes;
using POCTest.Models;
using POCTest.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace POCTest.Controllers
{
    public class HomeController : Controller
    {
        private readonly IQuizRepository _quizRepository;

        public HomeController(IQuizRepository qRepository)
        {
            _quizRepository = qRepository;
        }

        // GET: /<controller>/
        [AcceptVerbs("GET")]
        public IActionResult Index()
        {
            return View();
            
            //return "This is the Index page";

            ////var questions = _questionRepository.GetQuestions();

            ////var homeViewModel = new HomeViewModel()
            ////{
            ////    Title = "WashU Quiz Questions",
            ////    Questions = questions.ToList()
            ////};

            ////return View(homeViewModel);
        }

        // GET: /Home/Questions
        [AcceptVerbs("GET")]
        public IActionResult Questions()
        {
            var questions = _quizRepository.GetQuestions();

            var homeViewModel = new HomeViewModel()
            {
                Title = "WashU Quiz Questions",
                Questions = questions.ToList()
            };

            return View(homeViewModel);
        }

        // GET: /Home/Answers
        [AcceptVerbs("GET")]
        public IActionResult Answers()
        {
            var answers = _quizRepository.GetAnswers();

            var homeViewModel = new HomeViewModel()
            {
                Title = "WashU Quiz Answers",
                Answers = answers.ToList()
            };

            return View(homeViewModel);
        }

        // GET: /Home/Question/{Id}
        [AcceptVerbs("GET")]
        public IActionResult Question(int Id)
        {
            var question = _quizRepository.GetQuestion(Id);

            var homeViewModel = new HomeViewModel()
            {
                _question = question
            };

            return View(homeViewModel);
        }

        // GET: /Home/AddQuestion
        [AcceptVerbs("GET")]
        public IActionResult AddQuestion()
        {   
            return View();
        }

        // POST: /Home/AddQuestion
        [AcceptVerbs("POST")]
        [ValidateAntiForgeryToken]
        public IActionResult AddQuestion(Question newQuestion)
        {
            newQuestion.CreatedDate = DateTime.Now;
            _quizRepository.SubmitQuestion(newQuestion);

            return RedirectToAction("QuestionSubmitted");
        }

        
        [AcceptVerbs("GET")]
        public IActionResult QuestionSubmitted()
        {
            return View();
        }
    }
}
