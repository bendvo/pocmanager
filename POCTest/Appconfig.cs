﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POCTest
{
    public static class Appconfig
    {
        public static IConfiguration config;

        public static string GetConfigValue(string key)
        {
            return config.GetValue<string>(key, "");
        }
    }
}
